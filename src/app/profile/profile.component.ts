import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user-service';
import { UserModel } from '../models/user-model';
import { ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ChangePasswordModel } from '../models/change-password-model';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	@ViewChild('userDetailsForm') userForm: NgForm
	public user: UserModel;
	private username: string;
	private firstname: string;
	private lastname: string;
	private email: string;

	private detailsErrorMessage: string;
	private passwordErrorMessage: string;
	private detailsErrorVisible: boolean;
	private passwordErrorVisible: boolean;

	constructor(private userService: UserService) { }

	ngOnInit() {
		this.userService.getUserInfo().then(res => {
			console.log('profile componens res: ');
			console.log(res);
			this.username = res["Username"];
			this.firstname = res["Firstname"];
			this.lastname = res["Lastname"];
			this.email = res["Email"];
			this.mapPropertiesToForm();
			console.log(this.userForm);
		}).catch(err => {
			console.log('profile component err: ');
			console.log(err);
		});
	}
	private mapPropertiesToForm() {
		this.userForm.setValue({
			Username: this.username,
			Firstname: this.firstname,
			Lastname: this.lastname,
			Email: this.email
		});
	}

	private setDetailsError(message: string) {
		this.detailsErrorMessage = message;
		this.detailsErrorVisible = true;
	}

	private setPasswordError(message: string) {
		this.passwordErrorMessage = message;
		this.passwordErrorVisible = true;
	}

	private removeDetailsError() {
		this.detailsErrorMessage = "";
		this.detailsErrorVisible = false;
	}

	private removePasswordError() {
		this.passwordErrorMessage = "";
		this.passwordErrorVisible = false;
	}

	private changeDetails(detailsForm: NgForm) {
		console.log(detailsForm);
		this.removeDetailsError();
		var obj = detailsForm.value;
		var user = new UserModel();
		user.Username = obj["Username"];
		user.Firstname = obj["Firstname"];
		user.Lastname = obj["Lastname"];
		user.Email = obj["Email"];
		this.userService.changeUserDetails(user).then(res => {
			console.log( "s: " + res);
		}).catch(err => {
			console.log("e: " + err);
			this.setDetailsError(err);
		});
	}

	private changePassword(changePasswordForm: NgForm){
		this.removePasswordError();
		var obj = changePasswordForm.value;
		var oldPass = obj['OldPassword'];
		var newPass = obj['NewPassword'];
		var confPass = obj['ConfirmNewPassword'];
		let model = new ChangePasswordModel(oldPass, newPass, confPass);
		this.userService.changeUserPassword(model).then(res => {
			console.log("f: ", res);
		}).catch(err => {
			console.log("f: ", err);
			this.setPasswordError(err);
		})
	}
}

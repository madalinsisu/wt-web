import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AuthenticationService} from "../services/authentication-service";
import { RegisterModel } from '../models/register-model';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../services/global-service.service';
import { HeaderComponent } from '../header/header.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public noError: boolean;
  public errorMessage: string;
  constructor(private authService : AuthenticationService,
              private router: Router,
              private globalService: GlobalServiceService) {
   }

  ngOnInit() {
    this.noError = true;
  }

  public getErrorText(): string{
    return this.errorMessage;
  }

  register(registerForm: NgForm){
    this.noError = true;
    this.errorMessage = '';
    var obj = registerForm.value;
    var registerModel = new RegisterModel(obj.firstname, obj.lastname, obj.username, obj.email, obj.password, obj.passwordConfirm);
      this.authService.register(registerModel, this).then(() =>{
        if(this.authService.getToken() !== null){
          console.log("Success!")
          this.router.navigate([this.globalService.getRouteForPage('Home')]);
          window.location.reload();
        }
        else{
          console.log("not ok");
          this.noError = false;
        }
      });
  }
}

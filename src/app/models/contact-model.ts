export class ContactModel{
    constructor(public Name:string,
    public Email:string,
    public Subject: string,
    public Content: string){
    }
}
export class UserModel{
    Id: number;
    Firstname: string;
    Lastname: string;
    Username: string;
    Email: string;
}
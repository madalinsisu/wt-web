export class RegisterModel{

    constructor(public Firstname: string,
                public Lastname: string,
                public Username: string,
                public Email: string,
                public Password: string,
                public ConfirmPassword: string) {
    }
}
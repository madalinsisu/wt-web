export class ChangePasswordModel{
    private OldPassword: string;
    private NewPassword: string;
    private ConfirmNewPassword;

    constructor(oldPassword: string, newPassword: string, confirmPassword: string){
        this.OldPassword = oldPassword;
        this.NewPassword = newPassword;
        this.ConfirmNewPassword = confirmPassword;
    }
}
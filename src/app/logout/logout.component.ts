import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication-service';
import { GlobalServiceService } from '../services/global-service.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router,
    private authService : AuthenticationService,
    private globalService: GlobalServiceService) { }

  ngOnInit() {
    this.authService.logout();
    this.router.navigate([this.globalService.getRouteForPage('Home')]) 
  }
}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContactModel } from '../models/contact-model';
import { config } from '../../../protractor.conf';
import { ContactService } from '../services/contact-service';
import { GlobalServiceService } from '../services/global-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private contactService: ContactService,
    private router: Router,
    private globalService: GlobalServiceService) { }

  ngOnInit() {

  }

  public send(form: NgForm){
    var obj = form.value;
    var name = obj["Name"];
    var email = obj["Email"];
    var subject = obj["Subject"];
    var content = obj["Content"];
    var contactModel = new ContactModel(name, email, subject, content);
    this.contactService.sendContactMessage(contactModel).then(res => {
      this.router.navigate([this.globalService.getRouteForPage('Contact')]);
        window.location.reload();
    })
    .catch(err => {
      console.log("err: " + err);
    })
  }
}

import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { RoutesConstants } from "../constants/routes-constants";
import { ContactModel } from "../models/contact-model";
import { HttpParams } from "@angular/common/http/src/params";

@Injectable()
export class ContactService implements OnInit {


    constructor(private http: HttpClient,
        private routesConstants: RoutesConstants) {

    }

    ngOnInit() {

    }

    public sendContactMessage(contactModel: ContactModel) {
        var url = this.routesConstants.serviceUrl + this.routesConstants.contact;
        const headers = new HttpHeaders();
        headers.set('content-type', 'application/x-www-form-urlencoded');
        let promise = new Promise((resolve, reject) => {
            this.http.post(url, contactModel, { headers })
                .toPromise()
                .then(res => {
                    console.log("Success contact")
                    resolve(res);
                },
                err => {
                    console.log('Failed contact');
                    reject(err['error']);
                })
        });
        return promise;
    }
}
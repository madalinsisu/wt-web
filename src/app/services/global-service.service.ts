import { Injectable } from '@angular/core';
import { UserModel } from '../models/user-model';
import { ConstantsService } from '../constants/constants.service';
import { AuthenticationService } from './authentication-service';

@Injectable()
export class GlobalServiceService {

  currentUser: UserModel;
  constructor(private constantsService: ConstantsService,
  private authService: AuthenticationService) {
   }

  getLeftMenuItems() {
    var result = [
      this.constantsService.homeButton
    ];

    return result;
  }

  getRightMenuItems(){
    var result: string[] = [];
    if(this.authService.isAuthenticated()){
      result.push(this.constantsService.profileButton);
    }
    result.push(this.constantsService.contactBUtton);
    var logItem = this.authService.isAuthenticated() ? this.constantsService.logoutButton:
    this.constantsService.loginButton;
    result.push(logItem);
    if(!this.authService.isAuthenticated()){
      result.push(this.constantsService.registerButton);
    }
    
    return result;
  }

  public getRouteForPage(page:  string){
		var result = this.constantsService.routes[page];
		return "/" + result;
  }
  
  public getReversedRoute(page: string){
    var result = this.constantsService.reversedRoutes[page];
    return result;
  }
}

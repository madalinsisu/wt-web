import { Injectable } from '@angular/core';
import {RoutesConstants} from "../constants/routes-constants";
import {HttpClientModule, HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { RegisterModel, } from '../models/register-model'
import { LoginModel, } from '../models/login-model'
import { RegisterComponent } from '../register/register.component';
import { LoginComponent } from '../login/login.component';

@Injectable()
export class AuthenticationService{
    
    constructor(private http: HttpClient,
    private routesConstants: RoutesConstants){
    }

    public login(loginModel: LoginModel, loginComponent: LoginComponent){
        var url = this.routesConstants.serviceUrl + this.routesConstants.login;
        const headers =new HttpHeaders() ;
        headers.set('content-type', 'application/x-www-form-urlencoded');
        let params = new HttpParams();
        params = params.set('username', loginModel.Username);
        params = params.set('password', loginModel.Password);
        let promise = new Promise((resolve, reject) => {
            this.http.post(url, params, {headers})
            .toPromise()
            .then(res =>  {
                localStorage.setItem('token', res.toString());
                resolve();
            },
        err => {
            loginComponent.errorMessage = err.error;
                loginComponent.noError = false;
                this.removeToken();
        })
        });
        return promise;
    }

    public register(registerModel: RegisterModel, registerComponent: RegisterComponent){
        var url = this.routesConstants.serviceUrl + this.routesConstants.register;
        const headers = new HttpHeaders();
        headers.set('content-type', 'application/x-www-form-urlencoded');
        let params = new HttpParams();
        params = params.set('firstname', registerModel.Firstname);
        params = params.set('lastname', registerModel.Lastname);
        params = params.set('username', registerModel.Username);
        params = params.set('email', registerModel.Email);
        params = params.set('password', registerModel.Password);
        params = params.set('confirmpassword', registerModel.ConfirmPassword);
        let promise = new Promise((resolve, reject) => {
            this.http.post(url, params, {headers})
            .toPromise()
            .then(res =>  {
                localStorage.setItem('token', res.toString());
                resolve();
            },
        err => {
            registerComponent.errorMessage = err.error;
                registerComponent.noError = false;
                this.removeToken();
        })
        });
        return promise;
    }

    public saveToken(token: string){

    }

    public removeToken(){
        if(localStorage.getItem('token') !== null){
            localStorage.removeItem('token');
        }
    }

    public getToken(){
        return localStorage.getItem('token');
    }

    public onAuthenticationSucceeded(){
        
    }

    public logout(){
        this.removeToken();
        window.location.reload();
    }

    public isAuthenticated(): boolean{
        return localStorage.getItem('token') !== null;
    }
}
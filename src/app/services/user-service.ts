import { Injectable, OnInit } from '@angular/core';
import { UserModel } from '../models/user-model';
import { ConstantsService } from '../constants/constants.service';
import { AuthenticationService } from './authentication-service';
import { RoutesConstants } from '../constants/routes-constants';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { ChangePasswordModel } from '../models/change-password-model';

@Injectable()
export class UserService implements OnInit {

    constructor(private authService: AuthenticationService,
        private http: HttpClient,
        private routesConstants: RoutesConstants) {

    }

    ngOnInit() {

    }

    public getUserInfo() {
        var url = this.routesConstants.serviceUrl + this.routesConstants.profile;
        var authorization = 'basic ' + this.authService.getToken();
        const headers = new HttpHeaders({ 'Authorization': authorization });
        let promise = new Promise((resolve, reject) => {
            this.http.get<UserModel>(url, { headers })
                .toPromise()
                .then(res => {
                    resolve(res);
                },
                err => {
                    console.log('user service err');
                })
        });
        return promise;
    }

    public changeUserDetails(user: UserModel) {
        var url = this.routesConstants.serviceUrl + this.routesConstants.profile;
        var authorization = 'basic ' + this.authService.getToken();
        console.log(authorization);
        const headers = new HttpHeaders({ 'Authorization': authorization });
        let promise = new Promise((resolve, reject) => {
            this.http.put(url, user, { headers })
                .toPromise()
                .then(res => { console.log('ss: ' + res); resolve(res); },
                err => {
                    console.log("ee: " + err["error"]);
                    reject(err['error']);
                });
        });
        return promise;
    }

    public changeUserPassword(model: ChangePasswordModel) {
        var url = this.routesConstants.serviceUrl + this.routesConstants.changePassword;
        var authorization = 'basic ' + this.authService.getToken();
        const headers = new HttpHeaders({ 'Authorization': authorization });
        let promise = new Promise((resolve, reject) => {
            this.http.post(url, model, { headers })
                .toPromise()
                .then(res => {
                    console.log("dsfdfsdfsd"); resolve(res);
                },
                err => {
                    reject(err['error']);
                });
        });
        return promise;
    }
}

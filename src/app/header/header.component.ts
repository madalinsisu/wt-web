import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from '../services/global-service.service';
import { ConstantsService } from '../constants/constants.service';
import { Router } from '@angular/router';
import { ActivationEnd, NavigationEnd, NavigationStart } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean = false;
  leftItems: string[];
  rightItems: string[];
  allMenuItems: string[];
  activeBarItem: string;
  constructor(private router: Router,
  private globalService: GlobalServiceService,
  private  constantsService: ConstantsService) { 
    this.router.events.forEach((val) =>{
      if(val instanceof NavigationStart){
        var activeHeader = val.url.slice(1, val.url.length);
        var item = this.getReversedRoute(activeHeader);
         this.selectActiveItem(item);
      }
    });
    console.log("Header's constructor was called");
  }

  ngOnInit() {
    this.leftItems = this.globalService.getLeftMenuItems();
    this.rightItems = this.globalService.getRightMenuItems();
    this.allMenuItems = this.leftItems.concat(this.rightItems);
    this.activeBarItem = this.allMenuItems[0];
  }

  selectActiveItem(item){
    if(item === this.constantsService.logoutButton){
      this.router.navigate[this.globalService.getRouteForPage('Log out')] 
    }
    else{
      this.activeBarItem = item;
    }
  }

  isActive(item){
    return item == this.activeBarItem;
  }

  isRightItem(item){
    return this.rightItems.some(x => x=== item);
  }

  getRoute(item: string){
    var result = this.globalService.getRouteForPage(item);
    return result;
  }

  getReversedRoute(item: string){
    var result = this.globalService.getReversedRoute(item);
    return result;
  }
}

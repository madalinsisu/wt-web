import { Injectable } from '@angular/core';

@Injectable()
export class ConstantsService {

	public homeButton: string = "Home";
	public loginButton: string = " Log in";
	public registerButton: string = "Sign up";
	public logoutButton: string = "Log out";
	public profileButton: string = "My profile";
	public contactBUtton: string = "Contact";

	public routes: { [page: string] : string } = { };
	public reversedRoutes:{[page: string] : string } = { }
	constructor() {
		this.routes[""] = "";
		this.routes[this.homeButton] = "";
		this.routes[this.loginButton] = "login";
		this.routes[this.registerButton] = "signup";
		this.routes[this.logoutButton] = "logout";
		this.routes[this.profileButton] = "profile";
		this.routes[this.contactBUtton] = "contact";
		
		//------------------------------

		this.reversedRoutes[""] = this.homeButton;
		this.reversedRoutes["login"] = this.loginButton;
		this.reversedRoutes["signup"] = this.registerButton;
		this.reversedRoutes["logout"] = this.logoutButton;
		this.reversedRoutes["profile"] = this.profileButton;
		this.reversedRoutes["contact"] = this.contactBUtton;
	 }
}

import { Injectable } from '@angular/core';

@Injectable()
export class RoutesConstants {
    public serviceUrl: string = "http://localhost:58255/";
    public login: string = "api/authenticate";
    public register: string = "api/register";
    public profile: string = "api/profile";
    public changePassword: string = "api/changepassword";
    public contact: string = "api/contact";
}
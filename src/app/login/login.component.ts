import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AuthenticationService} from "../services/authentication-service";
import { LoginModel } from '../models/login-model';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../services/global-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public noError: boolean;
  public errorMessage: string;
  constructor(private authService : AuthenticationService,
    private router: Router,
    private globalService: GlobalServiceService) { 
    this.noError = true;
  }

  ngOnInit() {
    this.noError = true;
  }

  public getErrorText(): string{
    return this.errorMessage;
  }

  login(loginForm: NgForm){
    this.noError = true;
    this.errorMessage = '';
    var obj = loginForm.value;
    var loginModel = new LoginModel(obj.username, obj.password);
    
    this.authService.login(loginModel, this).then(() =>{
      if(this.authService.getToken() !== null){
        this.router.navigate([this.globalService.getRouteForPage('Home')]);
        console.log("Ok")
        window.location.reload();
      }
      else{
        console.log("not ok");
        this.noError = false;
      }
    });
  }
}
